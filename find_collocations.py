from nltk import *
from nltk.corpus import *
from nltk.collocations import *
import pandas as pd
import corpus as corpora

my_corpora = corpora.create_corpora()


def frequency_plus_part_of_speech_filter(input_text):
    bigram_finder = BigramCollocationFinder.from_words(brown.words())
    bi_grams = BigramAssocMeasures()

    tokenized = word_tokenize(input_text)
    tagged = pos_tag(tokenized)
    finder = BigramCollocationFinder.from_words(corpus.brown.tagged_words)
    bi_grams = list(bigrams(tagged))
    tri_grams = list(trigrams(tagged))
    filtered_bigrams, filtered_trigrams = [], []

    # filter by pos tags
    for bi_gram in bi_grams:
        if "NN" in bi_gram[1][1]:
            if "NN" in bi_gram[0][1] or "JJ" in bi_gram[0][1]:
                filtered_bigrams += [bi_gram]

    for tri_gram in tri_grams:
        if "NN" in tri_gram[2][1]:
            if "NN" in tri_gram[1][1] or "JJ" in tri_gram[1][1] or "IN" in tri_gram[1][1]:
                if "NN" in tri_gram[0][1] or "JJ" in tri_gram[0][1]:
                    filtered_trigrams += [tri_gram]

    # frequency check
    freq_dist = FreqDist(filtered_bigrams) + FreqDist(filtered_trigrams)
    freq_list = list(freq_dist.items())
    # sort by frequency
    freq_list.sort(key=lambda item: item[-1], reverse=True)

    return freq_list


def mean_and_variance(text, corpus):
    # ToDo implement this method
    return


def hypothesis_testing():
    # Bigram collocations using chi square testing for brown
    bigrams_measure = collocations.BigramAssocMeasures()
    finder_brown = collocations.BigramCollocationFinder.from_words(my_corpora.words())
    bigram_chi_table_brown = pd.DataFrame(list(finder_brown.score_ngrams(bigrams_measure.chi_sq)),
                                          columns=['bigram', 't']).sort_values(by='t', ascending=False)
    # filters
    bigrams_chi_brown = bigram_chi_table_brown[bigram_chi_table_brown.bigram.map(lambda x: right_types(x))]
    chi = []
    for coll in bigrams_chi_brown.values:
        chi += [coll[0]]
    return chi


# get english stopwords
ignored_words = corpus.stopwords.words('english')
# function to filter for ADJ/NN bigrams, stop words, length of words less than 3, white spaces


def right_types(ngram):
    if '-pron-' in ngram or 't' in ngram:
        return False
    for word in ngram:
        if word.lower() in ignored_words or word.isspace() or len(word) < 3:
            return False
    acceptable_types = ('JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNP', 'NNPS')
    second_type = ('NN', 'NNS', 'NNP', 'NNPS')
    tags = pos_tag(ngram)
    if tags[0][1] in acceptable_types and tags[1][1] in second_type:
        return True
    else:
        return False



