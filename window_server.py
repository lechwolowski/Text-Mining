from tkinter import *
from nltk import word_tokenize, sent_tokenize
from nltk.corpus import wordnet
import find_collocations


def check_text(*args):
    sentences = sent_tokenize(text.get(1.0, END))
    new_sentences = []
    for sentence in sentences:
        new_sentences += [modify_sentence(sentence)]
    text.delete(1.0, END)
    text.insert(1.0, '\n'.join(new_sentences))


def modify_sentence(sentence):
    words = word_tokenize(sentence)
    bi_grams = []

    for distance in range(1, 4):
        for i in range(0, len(words) - distance):
            bi_grams += [(words[i], words[i + distance], i, i + distance)]

    bi_grams = [item for item in bi_grams if len(item[0]) > 2 and len(item[1]) > 2]
    bi_grams = filter_bigrams(bi_grams)
    for bigram in bi_grams:
        words[bigram[2]] = first_synonyms(bigram[0], bigram[1])
        words[bigram[3]] = second_synonyms(bigram[0], bigram[1])
    return " ".join(words)


def filter_bigrams(bi_grams):
    result = []
    for bigram in bi_grams:
        first, second = bigram[0], bigram[1]
        if (first, second) not in collocations:
            result += [bigram]
    return result


def first_synonyms(first, second):
    first_syno = get_synonyms(first)
    for syn in first_syno:
        if (syn, second) in collocations:
            return syn
    return first


def second_synonyms(first, second):
    second_syno = get_synonyms(second)
    for syn in second_syno:
        if (first, syn) in collocations:
            return syn
    return second


def get_synonyms(word):
    syno = []
    for syn in wordnet.synsets(word):
        for l in syn.lemmas():
            syno.append(l.name())
    return syno


collocations = find_collocations.hypothesis_testing()

# initializing window
root = Tk()

# initializing Text field
text = Text(root, height=5, width=100)
text.pack(fill=BOTH)

# initializing button
button = Button(root, text="Check", command=check_text)
button.pack()

root.mainloop()
