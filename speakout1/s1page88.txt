Mexico’s Bike Revolution
It’s a quiet Sunday morning on the city’s Avenida Reforma. On weekdays there are tens of thousands of cars here, but today there’s not one car. In 2007, Mexico City closed its main road to cars on Sundays. That was the first big step towards becoming a bike-friendly city, and three years later, in 2010, it started a new bike sharing system, the ‘EcoBici’.
Now, with over 4,000 bikes, 276 cycle stations, and 87,000 users, Mexico City has one of the most successful bike share systems in the Americas. Cyclists can take a bike from one cycle station and leave
it at any other station in the city between the hours of 6.00a.m. and 00.30a.m. Users have to be over sixteen and pay 300 pesos by credit or debit card for a year’s use. They don’t have to pay anything for the first forty-five minutes of each journey.
Forty-nine-year-old businessman Mateo Reyes likes the scheme.
‘I only use my car when I’m too tired to cycle, but I go by bike when the traffic is bad. And the traffic is almost always bad.’ But he thinks it will take some time before cyclists and drivers learn to be happy sharing Mexico City’s roads.
