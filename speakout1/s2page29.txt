On 21st August, 1911, Leonardo da
Vinci’s Mona Lisa, one of the most
famous paintings in the world, was
stolen from the wall of the Louvre
Museum, in Paris. At first, the police
thought one of the guards might have
stolen the painting, but seventeen
days after the theft, they arrested poet
Guillaume Apollinaire. However, he
was released when police could find
no evidence that he had committed
the crime. Two years later, the real
thief, Vincenzo Peruggia, was arrested
in Italy. Peruggia had worked at the
museum, and had stolen the painting
because he was angry about how many
Italian paintings were on display in
France. He had planned to return the
painting to the Italian Uffizi gallery, in
Florence. The public was so excited at
the news of finding the
Mona Lisa that
the painting was displayed throughout Italy before it was returned to France in 1913.
