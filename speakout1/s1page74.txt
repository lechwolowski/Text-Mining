In Going Local, BBC journalists
go to cities around the world 
and try to see the city through
the eyes of a local person.
In this programme, travel journalist Carmen Roberts is in Hong Kong. She has three questions or challenges*
and she travels around the city to find the answers. There are two rules: no guidebooks and no tourists – so Carmen can only ask local people for help. Join Carmen as she leaves the skyscrapers, shopping centres and tourist places and tries to get to the heart
of Hong Kong.
