JUAN OLIVEIRA was born in Argentina, grew up
in Paraguay and now lives in Brazil. He says he loves the three countries equally, and he works in all three of them every day.
Juan is a tour guide in Foz do Iguaçu, a Brazilian town which is close to the borders of both Argentina and Paraguay. He takes tourists around the Iguaçu Falls, one of the great natural wonders of the world.
First, he shows tourists the waterfall from the Brazilian side. Then they cross the border to see the water from the Argentinian side. After that, they go on a boat trip which takes them under the waterfall. Finally, he takes them on the short journey to Ciudad del Este in Paraguay to do some tax-free shopping.
He says the Falls are amazing, especially in the rainy season. He sees them every day and he never gets tired of them.
