Performance of a lifetime?
Many years ago a crowd gathered outside the Paris Opera House to see a 
performance by one of the most famous opera singers of the time. Tickets had 
sold out weeks before, and opera fans had been looking forward to this epic 
moment ever since the performance was announced. It was a gorgeous spring evening, 
and everyone was wearing their finest clothes in celebration of the event.
In the moments before the curtain went up, the house lights dimmed slightly, 
and a hush fell over the audience as they saw something every theatregoer dreads.
A man in a suit was slowly walking out onto the stage. It was the house manager, 
and he announced to the audience that unfortunately, the famous singer had fallen 
ill and that her understudy, an unknown opera singer, would be performing in her place. 
A sense of disappointment pervaded the theatre. Some people got up and left.
Moments later, the curtain went up and the performance began. Throughout 
the nearly three-hour opera, the understudy, who 
had never appeared in a major opera before, gave the performance of her life. 
At the end of each major scene, when people usually applaud, there was literally 
no sound at all from the audience. Finally at the end of the opera, as the understudy 
sang her final notes and the orchestra played the last bars and stopped, there was 
only very faint applause. Suddenly, on one of the upper balconies, a little boy stood up.
