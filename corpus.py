from nltk.corpus.reader.plaintext import PlaintextCorpusReader


def create_corpora():
    corpus_dir = 'speakout1/'
    return PlaintextCorpusReader(corpus_dir, '.*')
